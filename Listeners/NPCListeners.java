package Listeners;

import java.util.HashMap;
import java.util.List;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.event.DespawnReason;
import net.citizensnpcs.api.event.NPCDespawnEvent;
import net.citizensnpcs.api.event.NPCPushEvent;
import net.citizensnpcs.api.event.NPCRemoveEvent;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import net.citizensnpcs.api.event.NPCSpawnEvent;
import net.citizensnpcs.api.npc.NPC;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.EntityEffect;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import com.earth2me.essentials.Essentials;
import com.earth2me.essentials.User;

import Merchants.BlackridgeMerchant;
import Merchants.CrasmeerMerchant;
import Merchants.FourthMerchant;
import Merchants.SpawnMerchant;
import Quests.ArcherQuest;
import Quests.BakerQuest;
import Quests.DemonStart;
import Quests.FelixQuest;
import Quests.FishermanQuest;
import Quests.JohnQuest;
import Quests.KnightQuest;
import Quests.LarryQuest;
import Quests.PaulQuest;
import Quests.QuestProcessor;
import Quests.ReinerQuest;
import Quests.SableHiltQuest;
import Quests.StableHand;
import Quests.StarterQuests;
import Quests.TravelerQuest;
import Quests.WitchQuest;
import Quests.WizardQuest;

public class NPCListeners implements Listener
{
	HashMap<Player, Inventory> bank = new HashMap(); 
	private JavaPlugin blacklance;
	public NPCListeners(JavaPlugin blacklance)
	{
		this.blacklance=blacklance;
	}
	
	@EventHandler
	public void theNeutrals(NPCRightClickEvent event) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException
	{
		Essentials ess = (Essentials) Bukkit.getServer().getPluginManager().getPlugin("Essentials");
		Player p = event.getClicker();
		int id = event.getNPC().getId();
		//Spawn Area Merchant
		if(id==20)
		{
			SpawnMerchant sm = new SpawnMerchant(event);
			sm.openInventory(event);
		}
		//Crasmeer Merchant
		if(id==23||id==178)
		{
			CrasmeerMerchant cm = new CrasmeerMerchant(event);
			cm.openInventory(event);
		}
		//Falkirk and BlackRidge Merchants
		if(id==194||id==399||id==411)
		{
			BlackridgeMerchant bm = new BlackridgeMerchant(event);
			bm.openInventory(event);
		}
		
		//==========================================================================================================
		if(id==484||id==506)
		{
			FourthMerchant fm = new FourthMerchant(event);
			fm.openInventory(event);
		}
		//==========================================================================================================
		
		//Bankers
		if(id==183||id==193)
		{
			Inventory inventory = Bukkit.createInventory(null, 27, "Bank");
			ItemStack[] inv = p.getEnderChest().getContents();
			inventory.setContents(inv);
			p.openInventory(inventory);
			bank.put(p, inventory);
		}
		//Innkeepers
		if(id==176||id==192||id==505)
		{
			User u = ess.getUser(p);
			u.setHome("home", p.getLocation());
			p.sendMessage(ChatColor.GOLD+"Your home has been set here!");
		}
		
		//BlackSmiths
		if(id==177)
		{
			QuestProcessor qp = new QuestProcessor(event,blacklance);
			qp.blacksmithCrasmeer();
		}
		
		//==========================================================================================================
		if(id==507)
		{
			QuestProcessor qp = new QuestProcessor(event,blacklance);
			qp.blacksmithRiverwood();
		}
		//==========================================================================================================
		
		//Farmer Giles
		if(id==436)
		{
			if(event.getClicker().getInventory().contains(Material.STONE_HOE)){
				event.getClicker().sendMessage(ChatColor.DARK_GREEN+"Farmer Giles: "+ChatColor.GREEN+"Seems like you have a hoe there.");
			}else{
			event.getClicker().sendMessage(ChatColor.DARK_GREEN+"Farmer Giles: "+ChatColor.GREEN+"Here's a hoe. We've got plenty of hoes around here.");
			event.getClicker().sendMessage(ChatColor.GOLD+"Farmer Giles winks and nudges you.");
			event.getClicker().getInventory().addItem(new ItemStack(Material.STONE_HOE));
			}
		}
		
		//Fisherman
		if(id==297)
		{
			if(event.getClicker().getInventory().contains(Material.FISHING_ROD)){
				event.getClicker().sendMessage(ChatColor.DARK_GREEN+"Fisherman: "+ChatColor.GREEN+"You have a fishing rod! Go catch some fish!");
			}else{
			event.getClicker().sendMessage(ChatColor.DARK_GREEN+"Fisherman: "+ChatColor.GREEN+"In need of a Fishing Rod eh? Here, have this.");
			event.getClicker().getInventory().addItem(new ItemStack(Material.FISHING_ROD));
			}
		}
	}
	@EventHandler
	public void questGivers(NPCRightClickEvent event) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException
	{
		Player p = event.getClicker();
		int id = event.getNPC().getId();
		//Spawn Guide
		if(id==133)
		{
			Quests.StarterQuests sq = new StarterQuests(event, blacklance);
			sq.starterQuest();
		}
		//Knight Quest
		if(id==132)
		{
			KnightQuest kq = new KnightQuest(event, blacklance, p);
			kq.knightsQuest();
		}
		//Witch Quest
		if(id==184){WitchQuest wq = new WitchQuest(event, blacklance, p);}
		//Mayor's Quest (demon chain start)
		if(id==195)
		{
			DemonStart ds = new DemonStart(event, blacklance, p);
		}
		//StableHand (Crasmeer)
		if(id==204){StableHand sh = new StableHand(event,blacklance,p);}
		
		//==========================================================================================================
		//ArcherQuest
		if(id==466){ArcherQuest aq = new ArcherQuest(event, blacklance, p);aq.archersQuest();}
		
		//BakerQuest
		if(id==467){BakerQuest bq = new BakerQuest(event, blacklance, p);bq.bakersQuest();}
		
		//FishermanQuest
		if(id==298){FishermanQuest fq = new FishermanQuest(event, blacklance, p);fq.fishermansQuest();}
		
		//JohnQuest
		if(id==469){JohnQuest jq = new JohnQuest(event, blacklance, p);jq.johnsQuest();}
		
		//LarryQuest
		if(id==483){LarryQuest lq = new LarryQuest(event, blacklance, p);lq.larrysQuest();}
		
		//PaulQuest
		if(id==485){PaulQuest pq = new PaulQuest(event, blacklance, p);pq.paulsQuest();}
		
		//ReinerQuest
		if(id==493){ReinerQuest rq = new ReinerQuest(event, blacklance, p);rq.reinersQuest();}
		
		//SableHiltQuest
		if(id==446){SableHiltQuest shq = new SableHiltQuest(event, blacklance, p);shq.sableHiltsQuest();}
		
		//TravelerQuest
		if(id==494){TravelerQuest tq = new TravelerQuest(event, blacklance, p);tq.TravelersQuest();}
		
		//WizardQuest
		if(id==504){WizardQuest wq = new WizardQuest(event, blacklance, p);wq.wizardsQuest();}
		
		//FelixQuest
		if(id==468){FelixQuest fq = new FelixQuest(event, blacklance, p); fq.felixsQuest();}
		//==========================================================================================================
		
	}
	@EventHandler(priority = EventPriority.MONITOR)
	public void stopSunlight(EntityCombustEvent event)
	{
		if(event.getDuration()==8){event.setDuration(0);}
	}
	@EventHandler
	public void stopPush(NPCPushEvent event)
	{
		event.setCancelled(true);
	}
	@EventHandler
	public void onItemSpawn(ItemSpawnEvent event) 
	{    
		if(!event.getEntity().getItemStack().hasItemMeta()){event.setCancelled(true);}
		else{if(!event.getEntity().getItemStack().getItemMeta().hasLore()){event.setCancelled(true);}}
	}   
	@EventHandler
	public void bankClose(InventoryCloseEvent event)
	{
		Player p = (Player) event.getPlayer();
		if(event.getInventory().getName().equals("Bank"))
		{
			ItemStack[] inv = bank.get(p).getContents();
			p.getEnderChest().setContents(inv);
		}
	}
}
