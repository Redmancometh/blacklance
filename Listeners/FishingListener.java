package Listeners;
import java.util.*;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
public class FishingListener implements Listener{
	@EventHandler
	public void onCaught(PlayerFishEvent e){
		
			e.getCaught().equals(null);
			
			ItemStack is = new ItemStack(Material.RAW_FISH);
			ItemMeta imIs = is.getItemMeta();
			imIs.setDisplayName("Slaughterfish");
			List<String> lore = new ArrayList<String>();
			lore.add("Dangerous, but delicious fish");
			imIs.setLore(lore);
			is.setItemMeta(imIs);
			e.getPlayer().getInventory().addItem(is);
			e.getPlayer().updateInventory();
	}
}
