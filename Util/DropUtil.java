package Util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.event.NPCDeathEvent;
import net.citizensnpcs.api.npc.NPC;
import net.minecraft.server.v1_7_R3.PacketPlayOutEntityDestroy;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_7_R3.entity.CraftItem;
import org.bukkit.craftbukkit.v1_7_R3.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.example.EntityHider;
import com.comphenix.example.EntityHider.Policy;

import BlackLance.BlackLance;
import Quests.QuestChecks;

public class DropUtil implements Listener
{
	private int hdmg;
	private int lowdmg;
	private int armwep;
	private int defense;
	private int level;
	private double type;
	private List<String> lore = new ArrayList<String>();
	private String prefix = "";
	private String suffix = "";
	private String value  = "Sell Value: ";
	private ItemStack drop = new ItemStack(Material.WOOD);
	private ItemMeta dropmeta = drop.getItemMeta();
	private Player p;
	private World w;
	private Location loc1;
	private EntityDeathEvent event;
	private boolean armor = false;
	private JavaPlugin blacklance;
	private Random t = new Random();
	
	public DropUtil(JavaPlugin blacklance)
	{
		this.blacklance=blacklance;
		if(event!=null)
		{
			this.event=event;
			this.p=this.event.getEntity().getKiller();
			this.w=this.event.getEntity().getWorld();
			this.loc1=this.event.getEntity().getLocation();
		}
	}
	public void clear(EntityDeathEvent event)
	{
		event.getDrops().clear();
    	event.getEntity().getEquipment().clear();
    	event.setDroppedExp(0);
	}
	
	public double dropDecider(int level, Player killer, Entity paul)
	{
		this.level=level;
		QuestChecks check = new QuestChecks(blacklance);
		check.checkQuestMob(paul, killer);
		double doDrop = Math.random();
		if(doDrop<(.30))
		{
			double dropWheel = Math.random();
			if(dropWheel<(.55)){dropCommon(level, killer, paul);}
			if(dropWheel>=(.55)&&dropWheel<=(.95)){dropUncommon(level, killer, paul);}
			if(dropWheel>(.99)&&dropWheel<(1)){dropRare(level, killer, paul);}
			/*if(dropWheel>=(.95)&&dropWheel<=(.98)){this.dropEpic(level, event);}
			if(dropWheel>(.98)){this.dropLegendary(level, event);}*/
		}
		if(doDrop>(.85)){dropPotion(killer, paul);}
		return doDrop;
	}
	public void dropPotion(Player killer, Entity paul)
	{
		ItemStack potion = new ItemStack(Material.POTION);
		ItemMeta potmeta = potion.getItemMeta();
		if(level<5)
		{
			potmeta.setDisplayName("Minor Healing Potion");
			lore.add("Heals Minor Damage");
			lore.add(ChatColor.BLUE+"Sell Value: 3");
			lore.add("PlayerI"+killer.getUniqueId());
		}
		if(level>5&&level<15)
		{
			potmeta.setDisplayName("Light Healing Potion");
			lore.add("Heals Light Damage");
			lore.add(ChatColor.BLUE+"Sell Value: 7");
			lore.add("PlayerI"+killer.getUniqueId());
		}
		
		//I just added these in case you want to add new potions. I hope I did it right. The code inside the "/====" comments is all I added here.
		//====================================================================================================================
		/*
		if(level>15&&level<25)
		{
			potmeta.setDisplayName("Medium Healing Potion");
			lore.add("Heals Medium Damage");
			lore.add(ChatColor.BLUE+"Sell Value: 11");
			lore.add("PlayerI"+killer.getUniqueId());
		}
		
		if(level>25)
		{
			potmeta.setDisplayName("High Healing Potion");
			lore.add("Heals High Damage");
			lore.add(ChatColor.BLUE+"Sell Value: 15");
			lore.add("PlayerI"+killer.getUniqueId());
		}
		*/
		//====================================================================================================================
		potmeta.setLore(lore);
		potion.setItemMeta(potmeta);
		lore.clear();
		CraftItem ci = (CraftItem) paul.getWorld().dropItem(paul.getLocation(), potion);
		hide(killer,ci);
	}
	public void dropCommon(int level, Player killer, Entity paul)
	{
		hdmg=t.nextInt(level+3)+1;if(hdmg>5)hdmg=5;
		lowdmg=t.nextInt(level)+1;if(lowdmg>4)lowdmg=4;
		armwep=t.nextInt(2);
		double subtype = Math.random();
		type=Math.random();
		if(lowdmg>hdmg){lowdmg-=(lowdmg-hdmg);}
		if(lowdmg==hdmg){hdmg++;}
		if(armwep==0)
		{
			if(level<=10)
			{
				if(type<=.15){drop.setType(Material.WOOD_SWORD);suffix="Wooden Sword";}
				if(type>.15&&type<=.31){drop.setType(Material.WOOD_AXE);suffix="Wooden Axe";}
				if(type>.31&&type<=.47){drop.setType(Material.SHEARS);suffix="Claws";}
				if(type>.47&&type<=.63){drop.setType(Material.IRON_HOE);suffix="Scythe";}
				
				if(type>.63&&type<=.79){drop.setType(Material.WOOD_SWORD);suffix="Dagger";}
				if(type>.79&&type<=1){drop.setType(Material.STONE_AXE);suffix="Hatchet";}

				if(hdmg<=level){prefix=ChatColor.GRAY+"Drunk-Made ";}
				if(hdmg>=level+1){prefix=ChatColor.GRAY+"Crappy ";}
				if(hdmg>=level+2){prefix=ChatColor.GRAY+"Low-Quality ";}
				
				if(hdmg>=level+3){prefix=ChatColor.GRAY+"Well-Made ";}
			}
			
			if(level>10&&level<=20)
			{
				if(type<=.10){drop.setType(Material.STONE_AXE);suffix="Stone Axe";}
				if(type>.10&&type<=.20){drop.setType(Material.STONE_SWORD);suffix="Stone Sword";}
				if(type>.20&&type<=.30){drop.setType(Material.SHEARS);suffix="Assassin Claws";}
				if(type>.30&&type<=.40){drop.setType(Material.IRON_HOE);suffix="Sickle";}
				
				if(type>.40&&type<=.50){drop.setType(Material.IRON_SWORD);suffix="Raiper";}
				if(type>.50&&type<=.60){drop.setType(Material.IRON_SWORD);suffix="Sabre";}
				if(type>.60&&type<=.70){drop.setType(Material.IRON_SWORD);suffix="Cutlass";}
				if(type>.70&&type<=.80){drop.setType(Material.WOOD_AXE);suffix="Felling Axe";}
				if(type>.80&&type<=.90){drop.setType(Material.STONE_AXE);suffix="Large Scythe";}
				if(type>.90&&type<=1){drop.setType(Material.IRON_HOE);suffix="Small Hammer";}
				
				if(hdmg<=level+1)
				{
					if(subtype>=.50){prefix=ChatColor.GRAY+"Poorly Made ";}
					if(subtype<.50){prefix=ChatColor.GRAY+"Broken ";}
				}
				if(hdmg>=level+2)
				{
					if(subtype>=.50){prefix=ChatColor.GRAY+"Badly Made ";}
					if(subtype<.50){prefix=ChatColor.GRAY+"Shoddy ";}
				}
				if(hdmg>=level+3)
				{
					if(subtype>=.50){prefix=ChatColor.GRAY+"Mediocre ";}
					if(subtype<.50){prefix=ChatColor.GRAY+"Dull ";}
				}
				hdmg+=2;
				lowdmg+=1;
			}
			
			if(level>20&&level<=30)
			{
				if(type<=.10){drop.setType(Material.STONE_AXE);suffix="Maul";}
				if(type>.10&&type<=.20){drop.setType(Material.IRON_SWORD);suffix="Iron Sword";}
				if(type>.20&&type<=.30){drop.setType(Material.SHEARS);suffix="Katar";}
				if(type>.30&&type<=.40){drop.setType(Material.IRON_HOE);suffix="Sickle";}
				
				if(type>.40&&type<=.50){drop.setType(Material.STONE_SWORD);suffix="Scimitar";}
				if(type>.50&&type<=.60){drop.setType(Material.IRON_SWORD);suffix="Long Sword";}
				if(type>.60&&type<=.70){drop.setType(Material.STONE_AXE);suffix="War Axe";}
				if(type>.70&&type<=.80){drop.setType(Material.STICK);suffix="Katana";}
				if(type>.80&&type<=.90){drop.setType(Material.STONE_SWORD);suffix="Bastard Sword";}
				if(type>.90&&type<=1){drop.setType(Material.STONE_AXE);suffix="Hand Scythe";}
				
				if(hdmg<=level+1)
				{
					if(subtype>=.50){prefix=ChatColor.GRAY+"Splintered ";}
					if(subtype<.50){prefix=ChatColor.GRAY+"Broken ";}
				}
				if(hdmg>=level+2)
				{
					if(subtype>=.50){prefix=ChatColor.GRAY+"Edentate ";}
					if(subtype<.50){prefix=ChatColor.GRAY+"Blunt ";}
				}
				if(hdmg>=level+3)
				{
					if(subtype>=.50){prefix=ChatColor.GRAY+"Jagged ";}
					if(subtype<.50){prefix=ChatColor.GRAY+"Barbed ";}
				}
				
				
				if(hdmg>=level+4)
				{
					if(subtype>=.50){prefix=ChatColor.GRAY+"Fine ";}
					if(subtype<.50){prefix=ChatColor.GRAY+"Superior ";}
				}
				
				
				hdmg+=4;
				lowdmg+=2;
			}
			
			
			if(level>30&&level<=40){
				
				if(type<=.10){drop.setType(Material.IRON_SWORD);suffix="Great Sword";}
				if(type>.10&&type<=.20){drop.setType(Material.IRON_AXE);suffix="Great Axe";}
				if(type>.20&&type<=.30){drop.setType(Material.STICK);suffix="Holy Staff";}
				if(type>.30&&type<=.40){drop.setType(Material.IRON_SWORD);suffix="Kodachi";}
				if(type>.40&&type<=.50){drop.setType(Material.STONE_SWORD);suffix="Nodachi";}
				if(type>.50&&type<=.60){drop.setType(Material.IRON_SWORD);suffix="Liberator";}
				if(type>.60&&type<=.70){drop.setType(Material.WOOD_SWORD);suffix="Tyrant Dragon";}
				if(type>.70&&type<=.80){drop.setType(Material.IRON_AXE);suffix="Dragon Bone Axe";}
				if(type>.80&&type<=.90){drop.setType(Material.WOOD_SWORD);suffix="Bayonet";}
				if(type>.90&&type<=1){drop.setType(Material.IRON_HOE);suffix="Great Hammer";}
				
				if(hdmg<=level+1)
				{
					if(subtype>=.50){prefix=ChatColor.GRAY+"Exquisite ";}
					if(subtype<.50){prefix=ChatColor.GRAY+"Flawless ";}
				}
				if(hdmg>=level+2)
				{
					if(subtype>=.50){prefix=ChatColor.GRAY+"Heavy ";}
					if(subtype<.50){prefix=ChatColor.GRAY+"Dense ";}
				}
				if(hdmg>=level+3)
				{
					if(subtype>=.50){prefix=ChatColor.GRAY+"Burning ";}
					if(subtype<.50){prefix=ChatColor.GRAY+"Freezing ";}
				}
				
				hdmg+=7;
				lowdmg+=5;
				
			}
			
			
			if(level>40&&level<=50){
				
				if(type<=.10){drop.setType(Material.IRON_SWORD);suffix="Volcanic Sword";}
				if(type>.10&&type<=.20){drop.setType(Material.IRON_AXE);suffix="Aurum";}
				if(type>.20&&type<=.30){drop.setType(Material.STONE_SWORD);suffix="Dragon Bone Sword";}
				if(type>.30&&type<=.40){drop.setType(Material.IRON_AXE);suffix="Faith's Edge";}
				if(type>.40&&type<=.50){drop.setType(Material.STONE_AXE);suffix="Dal'Thanaan";}
				if(type>.50&&type<=.60){drop.setType(Material.IRON_AXE);suffix="Griffon's Break";}
				if(type>.60&&type<=.70){drop.setType(Material.WOOD_AXE);suffix="Maetashear";}
				if(type>.70&&type<=.80){drop.setType(Material.IRON_SWORD);suffix="Ravager";}
				if(type>.80&&type<=.90){drop.setType(Material.STONE_SWORD);suffix="Warforger";}
				if(type>.90&&type<=1){drop.setType(Material.IRON_HOE);suffix="Battle Mace";}
				
				if(hdmg<=level+1)
				{
					if(subtype>=.50){prefix=ChatColor.GRAY+"Military ";}
					if(subtype<.50){prefix=ChatColor.GRAY+"The ";}
				}
				if(hdmg>=level+2)
				{
					if(subtype>=.50){prefix=ChatColor.GRAY+"Exalted ";}
					if(subtype<.50){prefix=ChatColor.GRAY+"Ancient ";}
				}
				if(hdmg>=level+3)
				{
					if(subtype>=.50){prefix=ChatColor.GRAY+"Grand ";}
					if(subtype<.50){prefix=ChatColor.GRAY+"Butcher's ";}
				}
				
				hdmg+=9;
				lowdmg+=7;
				
			}
			
			
			if(level>50&&level<=60){
				
				if(type<=.10){drop.setType(Material.IRON_SWORD);suffix="Gladius";}
				if(type>.10&&type<=.20){drop.setType(Material.IRON_AXE);suffix="Chopper";}
				if(type>.20&&type<=.30){drop.setType(Material.SHEARS);suffix="Demon Fang";}
				if(type>.30&&type<=.40){drop.setType(Material.IRON_AXE);suffix="Tomahawk";}
				if(type>.40&&type<=.50){drop.setType(Material.IRON_HOE);suffix="Morning Star";}
				if(type>.50&&type<=.60){drop.setType(Material.IRON_HOE);suffix="War Hammer";}
				if(type>.60&&type<=.70){drop.setType(Material.STONE_AXE);suffix="Masakari";}
				if(type>.70&&type<=.80){drop.setType(Material.IRON_SWORD);suffix="Shamshir";}
				if(type>.80&&type<=.90){drop.setType(Material.IRON_SWORD);suffix="Raid Sword";}
				if(type>.90&&type<=1){drop.setType(Material.IRON_HOE);suffix="Reaper's Scythe";}
				
				if(hdmg<=level+1)
				{
					if(subtype>=.50){prefix=ChatColor.GRAY+"Glorious ";}
					if(subtype<.50){prefix=ChatColor.GRAY+"Mighty ";}
				}
				if(hdmg>=level+2)
				{
					if(subtype>=.50){prefix=ChatColor.GRAY+"Petrified ";}
					if(subtype<.50){prefix=ChatColor.GRAY+"Magnificent ";}
				}
				if(hdmg>=level+3)
				{
					if(subtype>=.50){prefix=ChatColor.GRAY+"Hollowed ";}
					if(subtype<.50){prefix=ChatColor.GRAY+"Master ";}
				}
				
				hdmg+=11;
				lowdmg+=9;
				
			}
			
			value=value+((hdmg+lowdmg)-2);
		}
		if(armwep==1)
		{
			armor = true;
			if(level<=10)
			{
				defense = t.nextInt(level+2);
				if(type<=.25){drop.setType(Material.LEATHER_BOOTS);suffix=ChatColor.GRAY+"Leather Boots";}
				if(type<=.50&&type>.25){drop.setType(Material.LEATHER_CHESTPLATE);suffix=ChatColor.GRAY+"Leather Body Armor";}
				if(type>.50&&type<=.75){drop.setType(Material.LEATHER_HELMET);suffix=ChatColor.GRAY+"Leather Coif";}
				if(type>.75){drop.setType(Material.LEATHER_LEGGINGS);suffix=ChatColor.GRAY+"Leather Chaps";}
				if(defense<=level){prefix=ChatColor.GRAY+"Drunk-Made ";}
				if(defense>=level+1){prefix=ChatColor.GRAY+"Crummy ";}
				if(defense>=level+2){prefix=ChatColor.GRAY+"Low-Quality ";}
			}
			if(level>10&&level<=20)
			{
				defense = t.nextInt(level+3);
				if(type<=.25){drop.setType(Material.LEATHER_BOOTS);suffix=ChatColor.GRAY+"Leather Boots";}
				if(type<=.50&&type>.25){drop.setType(Material.LEATHER_CHESTPLATE);suffix=ChatColor.GRAY+"Leather Body Armor";}
				if(type>.50&&type<=.75){drop.setType(Material.LEATHER_HELMET);suffix=ChatColor.GRAY+"Leather Coif";}
				if(type>.75){drop.setType(Material.LEATHER_LEGGINGS);suffix=ChatColor.GRAY+"Leather Chaps";}
				if(defense<=level){prefix=ChatColor.GRAY+"Cracked ";}
				if(defense>=level+1){prefix=ChatColor.GRAY+"Threadbare ";}
				if(defense>=level+2){prefix=ChatColor.GRAY+"Torn ";}
				if(defense>=level+3){prefix=ChatColor.GRAY+"Moth-eaten ";}
			}
			if(level>20&&level<=30)
			{
				defense = t.nextInt(level+4);
				if(type<=.25){drop.setType(Material.CHAINMAIL_LEGGINGS);suffix=ChatColor.GRAY+"Iron Woven Leggings";}
				if(type<=.50&&type>.25){drop.setType(Material.CHAINMAIL_CHESTPLATE);suffix=ChatColor.GRAY+"Chain Weave Plackart";}
				if(type>.50&&type<=.75){drop.setType(Material.CHAINMAIL_HELMET);suffix=ChatColor.GRAY+"Reinforced Helmet";}
				if(type>.75){drop.setType(Material.CHAINMAIL_BOOTS);suffix=ChatColor.GRAY+"Chain Boots";}
				if(defense<=level){prefix=ChatColor.GRAY+"Sun Baked ";}
				if(defense>=level+1){prefix=ChatColor.GRAY+"Rusty ";}
				if(defense>=level+2){prefix=ChatColor.GRAY+"Degraded ";}
				if(defense>=level+3){prefix=ChatColor.GRAY+"Stiff ";}
			}
			if(level>30&&level<=40){
				defense = t.nextInt(level+5);
				if(type<=.25){drop.setType(Material.LEATHER_HELMET);suffix=ChatColor.GRAY+"Helmet";}
				if(type<=.50&&type>.25){drop.setType(Material.LEATHER_CHESTPLATE);suffix=ChatColor.GRAY+"Chestplate";}
				if(type>.50&&type<=.75){drop.setType(Material.LEATHER_LEGGINGS);suffix=ChatColor.GRAY+"Leggings";}
				if(type>.75){drop.setType(Material.LEATHER_BOOTS);suffix=ChatColor.GRAY+"Boots";}
				
				if(defense<=level){prefix=ChatColor.GRAY+"Light ";}
				if(defense>=level+1){prefix=ChatColor.GRAY+"Apprentice ";}
				if(defense>=level+2){prefix=ChatColor.GRAY+"Adept ";}
				if(defense>=level+3){prefix=ChatColor.GRAY+"Heavy ";}
			}
			
			if(level>40&&level<=50){
				defense = t.nextInt(level+6);
				if(type<=.25){drop.setType(Material.CHAINMAIL_HELMET);suffix=ChatColor.GRAY+"Helmet";}
				if(type<=.50&&type>.25){drop.setType(Material.CHAINMAIL_CHESTPLATE);suffix=ChatColor.GRAY+"Chestplate";}
				if(type>.50&&type<=.75){drop.setType(Material.CHAINMAIL_LEGGINGS);suffix=ChatColor.GRAY+"Leggings";}
				if(type>.75){drop.setType(Material.CHAINMAIL_BOOTS);suffix=ChatColor.GRAY+"Boots";}
				
				if(defense<=level){prefix=ChatColor.GRAY+"Roar ";}
				if(defense>=level+1){prefix=ChatColor.GRAY+"Rune ";}
				if(defense>=level+2){prefix=ChatColor.GRAY+"Defender's ";}
				if(defense>=level+3){prefix=ChatColor.GRAY+"White Spire ";}
			}
			
			if(level>50&&level<=60){
				defense = t.nextInt(level+7);
				if(type<=.25){drop.setType(Material.IRON_HELMET);suffix=ChatColor.GRAY+"Helmet";}
				if(type<=.50&&type>.25){drop.setType(Material.IRON_CHESTPLATE);suffix=ChatColor.GRAY+"Chestplate";}
				if(type>.50&&type<=.75){drop.setType(Material.IRON_LEGGINGS);suffix=ChatColor.GRAY+"Leggings";}
				if(type>.75){drop.setType(Material.IRON_BOOTS);suffix=ChatColor.GRAY+"Boots";}
				
				if(defense<=level+1){prefix=ChatColor.GRAY+"Dragon's ";}
				if(defense>=level+2){prefix=ChatColor.GRAY+"Healer's ";}
				if(defense>=level+3){prefix=ChatColor.GRAY+"Raging ";}
				if(defense>=level+4){prefix=ChatColor.GRAY+"Slayer's ";}
			}
			
			if(defense>1){value=value+(defense-1);}else{value=value+1;}
		}
			drop(killer, paul);
	}
	public void dropUncommon(int level, Player killer, Entity paul)
	{
		hdmg=t.nextInt(level+2)+2;
		lowdmg=t.nextInt(level)+1;
		armwep=t.nextInt(2);
		type=Math.random();
		if(lowdmg>hdmg){lowdmg-=(lowdmg-hdmg);}
		if(armwep==0)
		{
			
			if(level<=10)
			{
				if(type<.50){drop.setType(Material.WOOD_SWORD);suffix="Wooden Sword";}
				if(level>=4&&Math.random()>.50){hdmg+=1;}
				if(type>=.50&&type<=.65){drop.setType(Material.IRON_SWORD);suffix="Iron Sword";}
				if(type>.65){drop.setType(Material.WOOD_AXE);suffix="Wooden Axe";}
				if(hdmg<=level+1){prefix="Decent ";}
				if(hdmg>=level+2){prefix="Average ";}
				if(hdmg>=level+3){prefix="Above-Average ";}
				if(hdmg>=level+4){prefix="Dull ";}

			}
			
			if(level>10&&level<=20){
				if(type<.50){drop.setType(Material.WOOD_SWORD);suffix="Wooden Sword";}
				if(level>=4&&Math.random()>.50){hdmg+=1;}
				if(type>=.50&&type<=.65){drop.setType(Material.STONE_SWORD);suffix="Stone Sword";}
				if(type>.65){drop.setType(Material.WOOD_AXE);suffix="Wooden Axe";}
				
				if(hdmg<=level+1){prefix="Mediocre ";}
				if(hdmg>=level+2){prefix="Suitable ";}
				if(hdmg>=level+3){prefix="Unblemished ";}
				if(hdmg>=level+4){prefix="Sharp ";}
			}
			
			if(level>20&&level<=30){
				if(type<.50){drop.setType(Material.IRON_SWORD);suffix="Iron Sword";}
				if(level>=4&&Math.random()>.50){hdmg+=1;}
				if(type>=.50&&type<=.65){drop.setType(Material.IRON_SPADE);suffix="Iron Spear";}
				if(type>.65){drop.setType(Material.STONE_AXE);suffix="Stone Axe";}
				
				if(hdmg<=level+1){prefix="Amazing ";}
				if(hdmg>=level+2){prefix="Illustrious ";}
				if(hdmg>=level+3){prefix="Demolishing ";}
				if(hdmg>=level+4){prefix="Griswold's ";}
			}
			if(level>30&&level<=40){
				if(type<.50){drop.setType(Material.IRON_SWORD);suffix="Iron Sword";}
				if(level>=4&&Math.random()>.50){hdmg+=1;}
				if(type>=.50&&type<=.65){drop.setType(Material.IRON_SPADE);suffix="Iron Spear";}
				if(type>.65){drop.setType(Material.IRON_HOE);suffix="Iron Hammer";}
				
				if(hdmg<=level+1){prefix="Hunter's ";}
				if(hdmg>=level+2){prefix="Soldier's ";}
				if(hdmg>=level+3){prefix="Flawless ";}
				if(hdmg>=level+4){prefix="Monster Slayer ";}
			}
			if(level>40&&level<=50){
				if(type<.50){drop.setType(Material.IRON_SWORD);suffix="Iron Sword";}
				if(level>=4&&Math.random()>.50){hdmg+=1;}
				if(type>=.50&&type<=.65){drop.setType(Material.IRON_SPADE);suffix="Iron Spear";}
				if(type>.65){drop.setType(Material.STONE_AXE);suffix="Stone Axe";}
				
				if(hdmg<=level+1){prefix="Strange ";}
				if(hdmg>=level+2){prefix="One-Handed ";}
				if(hdmg>=level+3){prefix="Fast ";}
				if(hdmg>=level+4){prefix="Shiny ";}
			}
			if(level>50&&level<=60){
				if(type<.50){drop.setType(Material.IRON_SPADE);suffix="Long Spear";}
				if(level>=4&&Math.random()>.50){hdmg+=1;}
				if(type>=.50&&type<=.65){drop.setType(Material.IRON_HOE);suffix="Iron Mace";}
				if(type>.65&&type<=.85){drop.setType(Material.IRON_AXE);suffix="Iron Axe";}
				if(type>.85){drop.setType(Material.IRON_SWORD);suffix="Iron Sword";}
				
				if(hdmg<=level){prefix="Conquerer's ";}
				if(hdmg>=level+1){prefix="Warrior's ";}
				if(hdmg>=level+2){prefix="Heroic ";}
				if(hdmg>=level+3){prefix="Severing ";}
			}
			value=value+((hdmg+lowdmg)-2);
		}
		if(armwep==1)
		{
			armor = true;
			if(level<=10)
			{
				defense = t.nextInt(level+2);
				if(type<=.25){drop.setType(Material.IRON_BOOTS);suffix="Basic Greaves";}
				if(type<=.50&&type>.25){drop.setType(Material.LEATHER_CHESTPLATE);suffix="Leather Body Armor";}
				if(type>.50&&type<=.75){drop.setType(Material.IRON_HELMET);suffix="Helmet";}
				if(type>.75){drop.setType(Material.LEATHER_LEGGINGS);suffix="Leather Chaps";}
				if(defense<=level+1){prefix="Mediocre ";}
				if(defense>=level+2){prefix="Average ";}
				if(defense>=level+3){prefix="Above-Average ";}
				defense+=1;
				if(defense>1){value=value+(defense-1);}else{value=value+1;}
			}
			if(level>10&&level<=25){
				defense = t.nextInt(level+4);
				if(type<=.25){drop.setType(Material.IRON_BOOTS);suffix="Basic Greaves";}
				if(type<=.50&&type>.25){drop.setType(Material.LEATHER_CHESTPLATE);suffix="Leather Body Armor";}
				if(type>.50&&type<=.75){drop.setType(Material.IRON_HELMET);suffix="Basic Helmet";}
				if(type>.75){drop.setType(Material.LEATHER_LEGGINGS);suffix="Leather Chaps";}
				if(defense<=level+1){prefix="Beaten ";}
				if(defense>=level+2){prefix="Standard ";}
				if(defense>=level+3){prefix="Solid ";}
				if(defense>=level+4){prefix="Reinforced ";}
				defense+=3;
				if(defense>1){value=value+(defense-1);}else{value=value+1;}
			}
			if(level>25&&level<=40){
				defense = t.nextInt(level+6);
				if(type<=.25){drop.setType(Material.IRON_BOOTS);suffix="Advanced Boots";}
				if(type<=.50&&type>.25){drop.setType(Material.LEATHER_CHESTPLATE);suffix="Coat";}
				if(type>.50&&type<=.75){drop.setType(Material.IRON_HELMET);suffix="Iron Med Helmet";}
				if(type>.75){drop.setType(Material.LEATHER_LEGGINGS);suffix="Swift Leggings";}
				if(defense<=level+1){prefix="Resplendent ";}
				if(defense>=level+2){prefix="Sovereign ";}
				if(defense>=level+3){prefix="Supreme ";}
				if(defense>=level+4){prefix="High-Quality ";}
				defense+=5;
				if(defense>1){value=value+(defense-1);}else{value=value+1;}
			}
			if(level>40&&level<=60){
				defense = t.nextInt(level+8);
				if(type<=.25){drop.setType(Material.LEATHER_BOOTS);suffix="Swift Boots";}
				if(type<=.50&&type>.25){drop.setType(Material.LEATHER_CHESTPLATE);suffix="Bandit Tunic";}
				if(type>.50&&type<=.75){drop.setType(Material.LEATHER_HELMET);suffix="Leather Hood";}
				if(type>.75){drop.setType(Material.LEATHER_LEGGINGS);suffix="Light Leggings";}
				if(defense<=level+1){prefix="Sun Keeper ";}
				if(defense>=level+2){prefix="The dead's ";}
				if(defense>=level+3){prefix="Sneaky ";}
				if(defense>=level+4){prefix="Superb ";}
				defense+=7;
				if(defense>1){value=value+(defense-1);}else{value=value+1;}
			}
		}
			drop(killer, paul);
			
	}
	public void dropRare(int level, Player killer, Entity paul)
	{
		double decider = Math.random();
		armwep=t.nextInt(2);
		if(armwep==0)
		{
			if(level<=10)
			{
				if(decider<=.10){prefix=(ChatColor.GREEN+"The Knicker");suffix="";drop.setType(Material.IRON_AXE);hdmg=6;lowdmg=4;}
				if(decider<=.40&&decider>.10){prefix=(ChatColor.GREEN+"GutRipper");suffix="";drop.setType(Material.STONE_SWORD);hdmg=10;lowdmg=2;}
				if(decider<=1&&decider>.40){prefix=(ChatColor.GREEN+"Armot's Spear");suffix="";drop.setType(Material.IRON_SPADE);hdmg=10;lowdmg=4;}
			//lowlvl-rare loot table
			}
			if(level>10&&level<=25)
			{
				if(decider<=.10){prefix=(ChatColor.GREEN+"Night's Edge");suffix="";drop.setType(Material.GOLD_AXE);hdmg=19;lowdmg=9;}
				if(decider<=.40&&decider>.10){prefix=(ChatColor.GREEN+"Nightbane");suffix="";drop.setType(Material.IRON_SWORD);hdmg=18;lowdmg=9;}
				if(decider<=1&&decider>.40){prefix=(ChatColor.GREEN+"Brainhacker");suffix="";drop.setType(Material.GOLD_AXE);hdmg=16;lowdmg=9;}
			}
			if(level>25&&level<35){
				if(decider<=.10){prefix=(ChatColor.GREEN+"Assassin's Blade");suffix="";drop.setType(Material.GOLD_SWORD);hdmg=22;lowdmg=14;}
				if(decider<=.40&&decider>.10){prefix=(ChatColor.GREEN+"Moonlight Axe");suffix="";drop.setType(Material.IRON_AXE);hdmg=20;lowdmg=16;}
				if(decider<=1&&decider>.40){prefix=(ChatColor.GREEN+"Samurai's Katana");suffix="";drop.setType(Material.IRON_SWORD);hdmg=24;lowdmg=18;}
			}
			if(level>35&&level<50){
				if(decider<=.10){prefix=(ChatColor.GREEN+"Daedric Short Sword");suffix="";drop.setType(Material.GOLD_SWORD);hdmg=25;lowdmg=19;}
				if(decider<=.40&&decider>.10){prefix=(ChatColor.GREEN+"Dragonbone Dagger");suffix="";drop.setType(Material.STONE_SWORD);hdmg=25;lowdmg=17;}
				if(decider<=1&&decider>.40){prefix=(ChatColor.GREEN+"Soul Stealer");suffix="";drop.setType(Material.WOOD_HOE);hdmg=24;lowdmg=23;}
			}
			if(level>50&&level<60){
				if(decider<=.10){prefix=(ChatColor.GREEN+"Daedric Long Sword");suffix="";drop.setType(Material.GOLD_SWORD);hdmg=40;lowdmg=30;}
				if(decider<=.40&&decider>.10){prefix=(ChatColor.GREEN+"Dark Liberator");suffix="";drop.setType(Material.IRON_AXE);hdmg=37;lowdmg=29;}
				if(decider<=1&&decider>.40){prefix=(ChatColor.GREEN+"Angelic Sword");suffix="";drop.setType(Material.IRON_SWORD);hdmg=38;lowdmg=30;}
			}
			value=value+((hdmg+lowdmg)+15);
		}
		if(armwep==1)
		{
			armor=true;
			if(level<=10)
			{
				if(decider<=.10){prefix=(ChatColor.GREEN+"Helm of Honor");suffix="";drop.setType(Material.IRON_HELMET);defense=11;}
				if(decider<=.20&&decider>.10){prefix=(ChatColor.GREEN+"Warboots");suffix="";drop.setType(Material.IRON_BOOTS);defense=12;}
				if(decider<=.40&&decider>.20){prefix=(ChatColor.GREEN+"Cuirass of the Wind");suffix="";drop.setType(Material.IRON_CHESTPLATE);defense=15;}
				if(decider<=1&&decider>.40){prefix=(ChatColor.GREEN+"Jade Infused Legplates");suffix="";drop.setType(Material.IRON_LEGGINGS);defense=11;}
			//lowlvl-rare loot table
			}
			if(level>10&&level<=25)
			{
				if(decider<=.10){prefix=(ChatColor.GREEN+"Helm of Valor");suffix="";drop.setType(Material.GOLD_HELMET);defense=14;}
				if(decider<=.20&&decider>.10){prefix=(ChatColor.GREEN+"Emporer's Sabatons");suffix="";drop.setType(Material.GOLD_HELMET);defense=15;}
				if(decider<=1&&decider>.20){prefix=(ChatColor.GREEN+"King's Breastplate");suffix="";drop.setType(Material.GOLD_CHESTPLATE);defense=17;}
			}
			if(level>20&&level<30)
			{
				if(decider<=.10){prefix=(ChatColor.GREEN+"Mercurial Helm");suffix="";drop.setType(Material.GOLD_HELMET);defense=14;}
				if(decider<=.20&&decider>.10){prefix=(ChatColor.GREEN+"Gladiators Treads");suffix="";drop.setType(Material.GOLD_BOOTS);defense=15;}
				if(decider<=.40&&decider>.20){prefix=(ChatColor.GREEN+"Helm of Bartuc");suffix="";drop.setType(Material.LEATHER_HELMET);defense=17;}
				if(decider<=1&&decider>.40){prefix=(ChatColor.GREEN+"Juggernaught Plates");suffix="";drop.setType(Material.IRON_CHESTPLATE);defense=17;}
			}
			if(level>30&&level<40){
				if(decider<=.10){prefix=(ChatColor.GREEN+"Shadow Helmet");suffix="";drop.setType(Material.GOLD_HELMET);defense=25;}
				if(decider<=.20&&decider>.10){prefix=(ChatColor.GREEN+"Legionnaire boots");suffix="";drop.setType(Material.GOLD_BOOTS);defense=27;}
				if(decider<=.40&&decider>.20){prefix=(ChatColor.GREEN+"Leggings of the Dead");suffix="";drop.setType(Material.LEATHER_BOOTS);defense=29;}
				if(decider<=1&&decider>.40){prefix=(ChatColor.GREEN+"Dark Sun Armor");suffix="";drop.setType(Material.IRON_CHESTPLATE);defense=30;}
			}
			if(level>40&&level<50){
				if(decider<=.10){prefix=(ChatColor.GREEN+"Shining Dragon Armor");suffix="";drop.setType(Material.GOLD_CHESTPLATE);defense=35;}
				if(decider<=.20&&decider>.10){prefix=(ChatColor.GREEN+"Scorpion Boots");suffix="";drop.setType(Material.IRON_BOOTS);defense=33;}
				if(decider<=.40&&decider>.20){prefix=(ChatColor.GREEN+"Templar Helmet");suffix="";drop.setType(Material.LEATHER_HELMET);defense=29;}
				if(decider<=1&&decider>.40){prefix=(ChatColor.GREEN+"Abyssal Leggings");suffix="";drop.setType(Material.IRON_LEGGINGS);defense=35;}
			}
			if(level>50&&level<60){
				if(decider<=.10){prefix=(ChatColor.GREEN+"Grand Master Legwraps");suffix="";drop.setType(Material.LEATHER_LEGGINGS);defense=38;}
				if(decider<=.20&&decider>.10){prefix=(ChatColor.GREEN+"Stormy Boots");suffix="";drop.setType(Material.IRON_BOOTS);defense=40;}
				if(decider<=.40&&decider>.20){prefix=(ChatColor.GREEN+"Fire Helmet");suffix="";drop.setType(Material.LEATHER_HELMET);defense=39;}
				if(decider<=1&&decider>.40){prefix=(ChatColor.GREEN+"Blue Moon Armor");suffix="";drop.setType(Material.IRON_CHESTPLATE);defense=40;}
			}
			value=value+(defense+15);
		}
		drop(killer, paul);
	}
	public void dropEpic(int level, EntityDeathEvent event)
	{
		int epicWheel=t.nextInt();
		if(level<10)
		{
			prefix=(ChatColor.DARK_PURPLE+"The Hallowed Scythe");suffix="";drop.setType(Material.IRON_HOE);hdmg=26;lowdmg=18;
		}
		if(level>10&&level<20)
		{
			prefix=(ChatColor.DARK_PURPLE+"Omen Claws");suffix="";drop.setType(Material.SHEARS);hdmg=20;lowdmg=13;
		}
		if(level>20&&level<30)
		{
			prefix=(ChatColor.DARK_PURPLE+"The Hallowed Scythe");suffix="";drop.setType(Material.IRON_HOE);hdmg=26;lowdmg=18;
		}
		value=value+((hdmg+lowdmg)+40);

	}
	public void dropLegendary(int level, EntityDeathEvent event)
	{
		//Legendary Loot table.
		int legendWheel=t.nextInt();
		if(level>=80){
			if(legendWheel<.40){prefix=(ChatColor.DARK_RED+"Elucidator");suffix="";drop.setType(Material.DIAMOND_SWORD);hdmg=100;lowdmg=90;}
			if(legendWheel>.40&&legendWheel<.80){prefix=(ChatColor.DARK_RED+"Dark Repulser");suffix="";drop.setType(Material.DIAMOND_SWORD);hdmg=100;lowdmg=90;}
			if(legendWheel>80&&legendWheel<1){prefix=(ChatColor.DARK_RED+"Excalibur");suffix="";drop.setType(Material.GOLD_SWORD);hdmg=100;lowdmg=100;}
		}
	}
	public void drop(Player killer, Entity paul)
	{
		if(armor==false){lore.add("Damage: "+lowdmg+"-"+hdmg);}
		if(armor==true){lore.add("Defense: "+defense);}
		lore.add(ChatColor.BLUE+value);
		dropmeta.setDisplayName(prefix+suffix+" ");
		drop.setDurability((short)0);
		armor=false;
		lore.add("PlayerI"+killer.getUniqueId());
		dropmeta.setLore(lore);		
		drop.setItemMeta(dropmeta);
		if(dropmeta.getDisplayName().equals("Helm of Bartuc"))
		{
			LeatherArmorMeta lm = (LeatherArmorMeta)drop.getItemMeta();
			lm.setColor(Color.RED);
			drop.setItemMeta(lm);
		}
		lore.clear();
		CraftItem ci = (CraftItem)paul.getWorld().dropItem(paul.getLocation(), drop);
		hide(killer,ci);
	}
	public void hide(Player p, CraftItem ci)
	{
		EntityHider eh = new EntityHider(blacklance, Policy.BLACKLIST);
		List<CraftItem> cil = new ArrayList<CraftItem>();
		List<Player> pl = new ArrayList<Player>();
		Location loc1 = p.getLocation();
		for(Iterator<Entity> iter = p.getNearbyEntities(loc1.getX(), loc1.getY(), loc1.getZ()).iterator(); iter.hasNext();)
		{
			Entity e = iter.next();
			if(e instanceof Player)
			{
				if(!e.equals(p)&&!CitizensAPI.getNPCRegistry().isNPC(e))
				{
					eh.hideEntity((Player)e, ci);
				}
			}
		}
	}

}